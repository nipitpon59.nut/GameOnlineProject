﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemDrop
{
    public static Vector2 RandomPosition(float yOffset)
    {
        var spawnPosition = new Vector2(Random.Range(-20.0f, 50.0f),yOffset);
        return spawnPosition;
    }
    public static Quaternion RandomRotation()
    {
        var spawnRotation = Quaternion.Euler(0.0f,0.0f,
        0.0f);
        return spawnRotation;
    }
}
