﻿using UnityEngine;
//using UnityStandardAssets.Characters.FirstPerson;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;
using UnityEngine.UI;

[RequireComponent(typeof(PhotonTransformView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
{

  
    int P = 1;
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;
    private void Awake()
    {

    }
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());

        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine)
        {
            info.Sender.TagObject = this.gameObject;
            LocalPlayerInstance = gameObject;
        }
        else
        {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponent<PlayerContorl>().enabled = false;
        }

    }

    void Update()
    {
        if (!photonView.IsMine)
            return;

        if (Input.GetKeyDown("space"))
        {
            Debug.Log("Get space bar");

        }
      
    }
  

}