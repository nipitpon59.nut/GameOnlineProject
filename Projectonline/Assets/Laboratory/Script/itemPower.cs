﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class itemPower : MonoBehaviourPun
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
 public void OnCollisionEnter2D(Collision2D other)
    {
     if (other.gameObject.CompareTag("PlayerRed"))
        {
            if(Input.GetKey(KeyCode.O))
            {
             GameObject[]listWall = GameObject.FindGameObjectsWithTag("House");
 for (int i=0;i<listWall.Length;i++){
      listWall[i].GetComponent<painting>().Powerup(1);
 }   
            photonView.RPC("PunRPCPower", RpcTarget.MasterClient);
            } 
            else if(Input.GetKey(KeyCode.P)){
             photonView.RPC("PunRPCPower", RpcTarget.MasterClient);
        }
        }
       
          if (other.gameObject.CompareTag("PlayerBlue"))
        {
           if(Input.GetKey(KeyCode.O))
            {
           GameObject[]listWall = GameObject.FindGameObjectsWithTag("House");
 for (int i=0;i<listWall.Length;i++){
      listWall[i].GetComponent<painting>().Powerup(1);
 }
            photonView.RPC("PunRPCPower", RpcTarget.MasterClient);
         } 
         else if(Input.GetKey(KeyCode.P)){
             photonView.RPC("PunRPCPower", RpcTarget.MasterClient);
        }
        }
    }
 [PunRPC]
 public void PunRPCPower() {

 Destroy(this.gameObject);
 }


 private void OnDestroy() {
 if (!photonView.IsMine)
 return;
 PhotonNetwork.Destroy(this.gameObject);
 }

}
