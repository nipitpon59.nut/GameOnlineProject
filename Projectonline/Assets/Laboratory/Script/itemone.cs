﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class itemone : MonoBehaviourPun
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("PlayerRed"))
        {
            if(Input.GetKey(KeyCode.O))
            {
            PlayerContorl otherSpeed = other.gameObject.GetComponent<PlayerContorl>();
            otherSpeed.Speedup(1);
            photonView.RPC("PunRPCpickitem", RpcTarget.MasterClient);
            } 
            else if(Input.GetKey(KeyCode.P)){
             photonView.RPC("PunRPCpickitem", RpcTarget.MasterClient);
        }
        }
       
          if (other.gameObject.CompareTag("PlayerBlue"))
        {
           if(Input.GetKey(KeyCode.O))
            {
            PlayerContorl otherSpeed = other.gameObject.GetComponent<PlayerContorl>();
            otherSpeed.Speedup(1);
            
            photonView.RPC("PunRPCpickitem", RpcTarget.MasterClient);
        }
         else if(Input.GetKey(KeyCode.P)){
             photonView.RPC("PunRPCpickitem", RpcTarget.MasterClient);
        }
        }
    }
    [PunRPC]
    private void PunRPCpickitem()
    {
        Destroy(this.gameObject);
    }
    private void OnDestroy()
    {
        if (!photonView.IsMine)
            return;
        PhotonNetwork.Destroy(this.gameObject);
    }
}
