﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
public class painting : MonoBehaviourPunCallbacks , IPunObservable
{
    public Sprite[] listNumbers;
    public int num = 0;
    public float timepainting = 0.0f;
    public bool red = false;
    public bool blue = false;
    // public PlayerContorl liquidcolor;
    public float _Red;
    public int Color;
    private bool IsRed;
    private bool IsBlue;
    public float Power;
    // Start is called before the first frame update
    void Start()
    {
        // liquidcolor=GetComponent<PlayerContorl>();
        // _Red=liquidcolor.liquid;
      Power=1f;
    }

    // Update is called once per frame
    void Update()
    {

        if (num > 2)
        {
            num = 0;
        }
        if (timepainting < -1f && red==true)
        {
            TakePaint(1, photonView.ViewID);
            TakeScoreRed(1, photonView.ViewID);
         
            timepainting = 0;
        }
        if (timepainting > 1f && blue==true)
        {
            TakePaint(2, photonView.ViewID);
            TakeScoreBlue(1, photonView.ViewID);
       
             timepainting = 0;
        }
       if(Color==1){
            IsRed=true;
       }
       if(Color==2){
            IsBlue=true;
       }

    }
    public void TakePaint(int paintingcolor, int OwnerNetID)
    {
        if (photonView != null){
            photonView.RPC("PunRPCTakedPaint", RpcTarget.All, paintingcolor, OwnerNetID);
        }
        else print("photonView is NULL.");
    }
      public void TakeScoreRed(int Scorered, int OwnerNetID)
    {
        if (photonView != null){
            photonView.RPC("PunRPCTakedScoreRed", RpcTarget.All, Scorered, OwnerNetID);
        }
        else print("photonView is NULL.");
    }
     public void TakeScoreBlue(int Scoreblue, int OwnerNetID)
    {
        if (photonView != null){
            photonView.RPC("PunRPCTakedScoreBlue", RpcTarget.All, Scoreblue, OwnerNetID);
        }
        else print("photonView is NULL.");
    }
    public void Takeswitch( ){
        
 photonView.RPC("swithNumber", RpcTarget.All);
            
    }
    
    [PunRPC]
    public void PunRPCTakedPaint(int paintingcolor, int OwnerNetID)
    {
        Debug.Log("Take Paint");
        this.gameObject.GetComponent<SpriteRenderer>().sprite = listNumbers[paintingcolor];
     Color=paintingcolor;
    }
     [PunRPC]
    public void PunRPCTakedScoreRed(int Scorered, int OwnerNetID)
    {
          PunNetworkManager.singleton.redscore++;
    }
    [PunRPC]
    public void PunRPCTakedScoreBlue(int Scoreblue, int OwnerNetID)
    {
          PunNetworkManager.singleton.bluescore++;
    }

     void OnTriggerEnter2D(Collider2D col)
    { 
      if (col.gameObject.tag == "PlayerRed")
        {
         red = true;
         blue=false;
        }
         if (col.gameObject.tag == "PlayerBlue")
        {
         red = false;
         blue=true;
        }
        
    }
   
    void OnTriggerStay2D(Collider2D col)
    { 
        PlayerContorl otherliquid = col.gameObject.GetComponent<PlayerContorl>();
        if (col.gameObject.tag == "PlayerRed"&& red == true)
        {
            if (Input.GetKey("space")  && PunNetworkManager.singleton.liquidred > 0 && Color!=1)
            {
               Timepaintingminu(0.3f*Power) ;
                Debug.Log("timepaintingRedDown");
              
                otherliquid.liquidRedDown(1.0f);
            }
        }
         else if (col.gameObject.tag == "PlayerBlue"&& blue == true)
        {
            if (Input.GetKey("space") && PunNetworkManager.singleton.liquidblue > 0&& Color!=2)
            {
                Timepaintingplus(0.3f*Power);
                Debug.Log("timepaintingBlueDown");
           
                otherliquid.liquidBlueDown(1.0f);
            }
        }
 
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(timepainting);
            stream.SendNext( PunNetworkManager.singleton.redscore);
            stream.SendNext( PunNetworkManager.singleton.bluescore);
        }
        else
        {
            timepainting = (float)stream.ReceiveNext();
             PunNetworkManager.singleton.redscore = (int)stream.ReceiveNext();
             PunNetworkManager.singleton.bluescore= (int)stream.ReceiveNext();
        }
    }

    public void Timepaintingplus(float amout) {
   timepainting += amout*Time.deltaTime;
}

   public void Timepaintingminu(float amout) {
   timepainting -= amout*Time.deltaTime;
}
 public void Powerup(float amout)
    {
        Power += amout;
    }
[PunRPC]
 public void swithNumber(){
  if(Color==1){
     TakePaint(2, photonView.ViewID);
     //Color=2;
}
if(Color==2){
     TakePaint(1, photonView.ViewID);
      // Color=1;
}
}
}

