﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class PlayerContorl : MonoBehaviourPun
{
     public SpriteRenderer sprite;
    public GameObject bar;
    Vector3 _Speed;
    public float speed;
    public float liquid;
    painting paint;
    public Rigidbody2D player;
    private bool lassthanzero = false;
      private Animator animator;
      public float scale;
    // Start is called before the first frame update
    void Start()
    {
        paint = GetComponent<painting>();
        player = GetComponent<Rigidbody2D>();
        sprite= GetComponent<SpriteRenderer>();
         animator = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        contorlmove();
        contorlbar();

    }
    void contorlbar()
    {
         _Speed=new Vector3(speed,0,0);
        if (Input.GetKey("space"))
        {
            Takepainting(true,photonView.ViewID);
            Debug.Log("Bar move");
            // bar.transform.localScale =new Vector3(2, 0.8f,1);
            if (bar.transform.localScale.x < 0.05f)
            {
                lassthanzero = true;
            }
            else
            {
                lassthanzero = false;
            }
            if (lassthanzero == false)
            {
                TakeBarDown(1.2F * Time.deltaTime,photonView.ViewID);
            }
        }
        else {
             Takepainting(false,photonView.ViewID);
            
        }
    }
    void contorlmove()
    {
        if (Input.GetKey(KeyCode.D))
        {
           // player.velocity = new Vector2(speed, 0);
           transform.Translate(_Speed* Time.deltaTime);
          // sprite.flipX = false;
           TakeFilp( false , photonView.ViewID);
        }
        if (Input.GetKey(KeyCode.A))
        {
           // player.velocity = new Vector2(-speed, 0);
            transform.Translate(-_Speed* Time.deltaTime);
            // sprite.flipX = true;
             TakeFilp( true , photonView.ViewID);
        }
       // float upMove = Time.deltaTime * 5;
       float upMove = 15f;
        if (Input.GetKeyDown(KeyCode.W)&&this.transform.position.y<6)
        {
            player.velocity = new Vector2(0,1*upMove);
           // transform.Translate(0, upMove, 0, Space.World);
        }

    }

    public void TakeBarDown( float scale,int OwnerNetID){
        if (photonView != null){
            photonView.RPC("PunRPCTakedBar", RpcTarget.All,scale, OwnerNetID);
        }
     }
    [PunRPC]
    public void PunRPCTakedBar(float scale, int OwnerNetID)
    {
        bar.transform.localScale -= new Vector3(scale, 0, 0);
    }
     public void TakeBarUp( float scaleup,int OwnerNetID){
        if (photonView != null){
            photonView.RPC("PunRPCTakedBarUP", RpcTarget.All,scaleup, OwnerNetID);
        }
     }
    [PunRPC]
    public void PunRPCTakedBarUP(float scaleup, int OwnerNetID)
    {
        bar.transform.localScale += new Vector3(scaleup, 0, 0);
    }

     public void Takepainting(bool painting, int OwnerNetID){
        if (photonView != null){
            photonView.RPC("PunRPCTakedpainting", RpcTarget.All, painting, OwnerNetID);
        }
     }

     public void TakeFilp(bool filping, int OwnerNetID)
    {
        if (photonView != null){
            photonView.RPC("PunRPCTakedFilp", RpcTarget.All, filping, OwnerNetID);
        }
       
    }
    
    [PunRPC]
    public void PunRPCTakedFilp(bool filping, int OwnerNetID)
    {
        sprite= GetComponent<SpriteRenderer>();
       this.sprite.flipX=filping;
    }
    [PunRPC]
    public void PunRPCTakedpainting(bool painting, int OwnerNetID)
    {
         animator = GetComponent<Animator>();
       this.animator.SetBool("Back", painting);
    }




    public void Speedup(int amout)
    {
        speed += amout;
    }
    public void liquidRedDown(float colorred)
    {
        PunNetworkManager.singleton.liquidred -= colorred * Time.deltaTime;
    }
    public void liquidBlueDown(float colorblue)
    {
        PunNetworkManager.singleton.liquidblue -= colorblue * Time.deltaTime;
    }
    public void liquidRedRefild(float RefildRed)
    {
        PunNetworkManager.singleton.liquidred += RefildRed;
        TakeBarUp(RefildRed,photonView.ViewID);
       
    }
    public void liquidBlueRefild(float RefildBlue)
    {
        PunNetworkManager.singleton.liquidblue += RefildBlue;
         TakeBarUp(RefildBlue,photonView.ViewID);
       // bar.transform.localScale += new Vector3(RefildBlue, 0, 0);
    }

}

