﻿using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;
using UnityEngine.UI;
using System;
public class PunNetworkManager : ConnectAndJoinRandom
{
    private bool have_red;
    private bool have_blue;
    public static PunNetworkManager singleton;
    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerRedPrefab;
    public GameObject GamePlayerBluePrefab;
    //item  
    [Header("The prefab item")]
    [Tooltip("The prefab item")]
    public float time_to_itemDrop = 5;
    public float time_to_bucketDrop = 5;
    public GameObject itemPrefab;
    public GameObject itemPowerPrefab;
    //bucket
    public GameObject bucketredPrefab;
    public GameObject bucketbluePrefab;
    //Gameseting
    public bool isGameStart = false;
    public bool isFirstSetting = false;
    public bool isGameOver = false;
    [Header("UI Seting")]
    [Tooltip(" UI")]
    public Button ButtonRed;
    public Button ButtonBlue;
    public Button Start;
    public GameObject UI;
    public GameObject StartS;
    public GameObject UIEnd;
  
    public float liquidred = 5;
    public float liquidblue = 5;
    [Header("Score")]
    [Tooltip("Score")]
    public int redscore;
    public int bluescore;
    private void StartGame()
    {
        Camera main = Camera.main;
        if (main.name == "Camera")
            main.gameObject.SetActive(false);

               Button btnr = ButtonRed.GetComponent<Button>();
            btnr.onClick.AddListener(TaskOnClickRed);
            Button btnb = ButtonBlue.GetComponent<Button>();
            btnb.onClick.AddListener(TaskOnClickBlue);
            Button startgameS = Start.GetComponent<Button>();
            startgameS.onClick.AddListener(TaskStart);
    }
    private void Awake()
    {
        singleton = this;
        StartGame();
        // isGameStart = true;
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("New Player. " + newPlayer.ToString());
    
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Camera.main.gameObject.SetActive(false);
        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            // Button btnr = ButtonRed.GetComponent<Button>();
            // btnr.onClick.AddListener(TaskOnClickRed);
            // Button btnb = ButtonBlue.GetComponent<Button>();
            // btnb.onClick.AddListener(TaskOnClickBlue);
            //    else if (PunTeams.PlayersPerTeam[PunTeams.Team.blue].Count < PunTeams.PlayersPerTeam[PunTeams.Team.red].Count)
            //     {
            //         PunNetworkManager.singleton.SpawnPlayerBlue();
            //         TeamExtensions.SetTeam(PhotonNetwork.LocalPlayer, (PunTeams.Team.blue));
            //         playersOnBlue++;
            //     } 
           
        }
        else
        {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }
    }
    public void SpawnPlayer()
    {
        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
        PhotonNetwork.Instantiate(GamePlayerRedPrefab.name, new Vector2(-18f, 0f), Quaternion.identity, 0);
   
    }
    public void SpawnPlayerBlue()
    {
        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
        PhotonNetwork.Instantiate(GamePlayerBluePrefab.name, new Vector2(44f, 0f), Quaternion.identity, 0);

    }
    void Update()
    { 
     
        if (PhotonNetwork.IsMasterClient != true)
              return;
            if(PunTeams.PlayersPerTeam[PunTeams.Team.blue].Count==PunTeams.PlayersPerTeam[PunTeams.Team.red].Count){

             StartS.SetActive(true);
                } 
                else{
              StartS.SetActive(false);
                }
        
           
     
        if (isGameStart == true)
        {  
            if (isFirstSetting == false)
                PunGameTimer.StartTime();    
              
        }
      cooldownitemdrop();
        cooldownitembucketreddrop(); 
        

       
        time_to_itemDrop -= Time.deltaTime;
        time_to_bucketDrop -= Time.deltaTime;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PunGameTimer.OnCountdownTimerHasExpired += OnCountdownTimerIsExpired;
    }
    public override void OnDisable()
    {
        base.OnDisable();
        PunGameTimer.OnCountdownTimerHasExpired -= OnCountdownTimerIsExpired;
    }
    private void OnCountdownTimerIsExpired()
    {
        Hashtable props = new Hashtable {
 {PunGameSetting.GAMEOVER, true}
 };
        PhotonNetwork.CurrentRoom.SetCustomProperties(props);
    }
    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
        object GameOver;
        if
       (propertiesThatChanged.TryGetValue(PunGameSetting.GAMEOVER, out GameOver))
        {
            Debug.Log("Game Over.");
            isGameOver = (bool)GameOver;
            Endgame();
        }
    }
    void cooldownitemdrop()
    {
        if (isFirstSetting == false)
        {
            isFirstSetting = true;
        }
        if (time_to_itemDrop < 0)
        {
            PhotonNetwork.InstantiateSceneObject(itemPrefab.name
             , itemDrop.RandomPosition(12f)
             , itemDrop.RandomRotation()
             , 0);
         PhotonNetwork.InstantiateSceneObject(itemPowerPrefab.name
             , itemDrop.RandomPosition(12f)
             , itemDrop.RandomRotation()
             , 0);
             

            time_to_itemDrop = 30;
        }

    }
    void cooldownitembucketreddrop()
    {
        if (time_to_bucketDrop < 0)
        {
            PhotonNetwork.InstantiateSceneObject(bucketredPrefab.name

                        , itemDrop.RandomPosition(-1f)
                        , itemDrop.RandomRotation()
                        , 0);
            PhotonNetwork.InstantiateSceneObject(bucketbluePrefab.name

                   , itemDrop.RandomPosition(-1f)
                   , itemDrop.RandomRotation()
                   , 0);
            time_to_bucketDrop = 15;

        }
    }

    void TaskOnClickRed()
    {
        PunNetworkManager.singleton.SpawnPlayer();
        TeamExtensions.SetTeam(PhotonNetwork.LocalPlayer, (PunTeams.Team.red));
        UI.SetActive(false);
        have_red = true;
       if(have_red==true){
       // StartS.SetActive(true);
     }
    }
    void TaskOnClickBlue()
    {
        PunNetworkManager.singleton.SpawnPlayerBlue();
        TeamExtensions.SetTeam(PhotonNetwork.LocalPlayer, (PunTeams.Team.blue));
        UI.SetActive(false);
        have_blue = true;
    }
      void TaskStart()
    {
      PunGameTimer.StartTime();
     StartS.SetActive(false);
    }
       
    void Endgame()
    {
        UIEnd.SetActive(true);
    }
}